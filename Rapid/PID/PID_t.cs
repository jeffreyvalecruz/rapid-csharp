﻿/* PID class architecture for a scalable PID system.
 *  
 * Author   : Jeffrey Vale Cruz
 * Date     : 29/7-2016
 * 
 * 
 * */
 namespace Rapid
{ 
    class PID_t
    {
        private string m_ID; 
        private double m_setPoint;
        private double m_outputRangeMin;
        private double m_outputRangeMax;
        private double m_KpGain;
        private double m_KiGain;
        private double m_KdGain;

        private long   m_tickCount = System.Environment.TickCount;

        private double m_registry;
        private double m_feedbackRegistry;

        /// <summary>
        /// Returns the elapsed time in seconds since last method call.
        /// </summary>
        /// <returns></returns>
        private double ElapsedSeconds()
        {
            long elapsedTicks = System.Environment.TickCount - m_tickCount;
            m_tickCount = System.Environment.TickCount;
            double elapsedSeconds = elapsedTicks / 1000;
            Log.Insert(Catagory.DEBUG, "{" + m_ID + "} Elapsed seconds since last call : " + elapsedSeconds.ToString());
            return elapsedSeconds;
        }

        /// <summary>
        /// Constructor for PID_t PID instance type.
        /// </summary>
        /// <param name="setPoint"></param>
        /// <param name="outputRangeMin"></param>
        /// <param name="outputRangeMax"></param>
        /// <param name="KpGain"></param>
        /// <param name="KiGain"></param>
        /// <param name="KdGain"></param>
        public PID_t(string ID, 
                     double setPoint, 
                     double outputRangeMin, 
                     double outputRangeMax, 
                     double KpGain, 
                     double KiGain, 
                     double KdGain)
        {
            Log.Insert(Catagory.DEBUG, "{" + m_ID + "} PID constructor called.");
            m_ID                = ID;
            m_setPoint          = setPoint;
            m_outputRangeMin    = outputRangeMin;
            m_outputRangeMax    = outputRangeMax;
            m_KpGain            = KpGain;
            m_KiGain            = KiGain;
            m_KdGain            = KdGain;
            Log.Insert(Catagory.INFO, "PID instance {ID: " + m_ID + 
                                      ", SetPoint: " + m_setPoint.ToString() + 
                                      ", OutputRangeMin: " + m_outputRangeMin + 
                                      ", OutputRangeMax: " + m_outputRangeMax + 
                                      ", Kp Gain: " + m_KpGain + 
                                      ", Ki Gain: " + m_KiGain + 
                                      ", Kd Gain: " + m_KdGain);
        }

        /// <summary>
        /// Calculates the PID output of a non-redundant input measurement. 
        /// </summary>
        /// <param name="inputValue"></param>
        /// <returns></returns>
        public double CalculateOutput(double inputValue)
        {
            Log.Insert(Catagory.DEBUG, "{" + m_ID + "} PID calculation requested.");
            m_registry = m_feedbackRegistry;
            m_feedbackRegistry = m_setPoint - inputValue;

            double elapsedSeconds = ElapsedSeconds();
            double output = ((m_feedbackRegistry * m_KpGain) + (((m_feedbackRegistry * elapsedSeconds) + 
                            (m_registry * elapsedSeconds)) * m_KiGain) +
                            ((m_feedbackRegistry - m_registry) / elapsedSeconds) * m_KdGain);

            if (output < m_outputRangeMin)
            {
                Log.Insert(Catagory.WARNING, "{" + m_ID + "} Output is coerced to : " + m_outputRangeMin.ToString());
                output = m_outputRangeMin;
            }
            else if (output > m_outputRangeMax)
            {
                Log.Insert(Catagory.WARNING, "{" + m_ID + "} Output is coerced to : " + m_outputRangeMax.ToString());
                output = m_outputRangeMax;
            }
            Log.Insert(Catagory.INFO, "{" + m_ID + "} Output calculated to : " + output.ToString());
            return output;
        }

        /// <summary>
        /// Calculates the PID output of a redundant input measurement.
        /// </summary>
        /// <param name="inputValues"></param>
        /// <param name="temperatureDivergencyMax"></param>
        /// <param name="uniformTemperature"></param>
        /// <returns></returns>
        public double CalculateOutput(double[] InputValues, 
                                      double RedundantInputDivergencyLimit, 
                                      out bool UniformInput)
        {
            System.Array.Sort(InputValues);

            UniformInput = System.Math.Abs(InputValues[InputValues.Length - 1] - InputValues[0]) <= RedundantInputDivergencyLimit;

            if (!UniformInput)
            {
                Log.Insert(Catagory.WARNING, "{" + m_ID + "} Input measurements are indicating non-uniformity dispersion of the output effect. " + 
                                             "Difference in highest and lowest input measurement is greater than temperatureDivergencyMax (" + RedundantInputDivergencyLimit +")");
            }

            return CalculateOutput(InputValues[InputValues.Length - 1]);
        }
    }
}
