﻿/* Rapid Dynamic Link Library.
 * General purpose multi-PID process handler.
 * 
 * Usage: 
 * To be called periodical from another program.
 * Calculates an output value based on the input value,
 * and a given setpoint, using P-I-D feedback evaluations. 
 * 
 * Author: Jeffrey Vale Cruz
 * Date  : 29/7-2016
 * 
 * */

namespace Rapid
{
    public static class Rapid
    {
        private static System.Collections.Generic.Dictionary<string, PID_t> PIDRegistry = new System.Collections.Generic.Dictionary<string, PID_t>();

        /// <summary>
        /// Initializes a PID instance, to be called for output calculations.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="setPoint"></param>
        /// <param name="outputRangeMin"></param>
        /// <param name="outputRangeMax"></param>
        /// <param name="KpGain"></param>
        /// <param name="KiGain"></param>
        /// <param name="KdGain"></param>
        public static void InitializePID(string ID, 
                                         double setPoint, 
                                         double outputRangeMin, 
                                         double outputRangeMax, 
                                         double KpGain, 
                                         double KiGain, 
                                         double KdGain)
        {
            PIDRegistry.Add(ID, new PID_t(ID, setPoint, outputRangeMin, outputRangeMax, KpGain, KiGain, KdGain));
        }

        /// <summary>
        /// Calculates the PID output of a non-redundant input measurement.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="inputValue"></param>
        /// <param name="outputValue"></param>
        /// <returns></returns>
        public static bool CalculateOutput(string ID, 
                                           double inputValue, 
                                           out double outputValue)
        {
            outputValue = 0;
            PID_t PID;

            if (PIDRegistry.TryGetValue(ID, out PID))
            {
                outputValue = PID.CalculateOutput(inputValue);
                return true;
            }
            else
            {
                Log.Insert(Catagory.ERROR, "No PID instance found with ID: " + ID);
                return false;
            }
            
        }

        /// <summary>
        /// Calculates the PID output of a redundant input measurement.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="inputValues"></param>
        /// <param name="outputValue"></param>
        /// <param name="uniformTemperature"></param>
        /// <returns></returns>
        public static bool CalculateOutput(string ID, 
                                           double[] InputValues, 
                                           double InputDivergencyLimit, 
                                           out double OutputValue, 
                                           out bool UniformInput)
        {
            OutputValue = 0;
            UniformInput = false;
            PID_t PID;

            if (PIDRegistry.TryGetValue(ID, out PID))
            {
                OutputValue = PID.CalculateOutput(InputValues, InputDivergencyLimit, out UniformInput);
                return true;
            }
            else
            {
                Log.Insert(Catagory.ERROR, "No PID instance found with ID: " + ID); 
                return false;
            }
        } 
    }
}
    
