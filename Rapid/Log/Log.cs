﻿/* Log class for general system logging.
 * 
 * Usage:
 * This class will post log entries to Windows output debug stream.
 * Use 'DebugView' to se the log inserts.
 *  
 * Author   : Jeffrey Vale Cruz
 * Date     : 29/7-2016
 * 
 * 
 * */
namespace Rapid
{
    enum Catagory { DEBUG, INFO, WARNING, ERROR}

    static class Log
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static void OutputDebugStringA(string debugString);

        public static void Insert(Catagory catagory, string EntryText)
        {
            OutputDebugStringA("[Rapid.PID] [" + catagory.ToString() + "] " + EntryText);
        }
    }
}
